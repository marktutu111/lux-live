(self["webpackChunkngx_admin"] = self["webpackChunkngx_admin"] || []).push([["common"],{

/***/ 56425:
/*!************************************************!*\
  !*** ./src/app/services/affiliates.service.ts ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AffiliateService": function() { return /* binding */ AffiliateService; }
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 79765);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 46782);
/* harmony import */ var _utils_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/api */ 52890);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _states_app_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../states/app.state */ 99921);






class AffiliateService {
    constructor(http, state) {
        this.http = http;
        this.state = state;
        this.subscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__.Subject();
    }
    ;
    add(data) {
        return this.http.post(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/affiliates/new`, data);
    }
    login(data) {
        return this.http.post(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/affiliates/login`, data);
    }
    updatePassword(data) {
        return this.http.put(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/affiliates/password/update`, data);
    }
    update(data) {
        return this.http.put(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/affiliates/update`, data);
    }
    getall() {
        this.http.get(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/affiliates/get`)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.takeUntil)(this.subscription$))
            .subscribe((response) => {
            if (response.success) {
                this.state.setAffiliates(response.data);
            }
        });
    }
    ngOnDestroy() {
        this.subscription$.next();
        this.subscription$.complete();
    }
}
AffiliateService.ɵfac = function AffiliateService_Factory(t) { return new (t || AffiliateService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_states_app_state__WEBPACK_IMPORTED_MODULE_1__.AppState)); };
AffiliateService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: AffiliateService, factory: AffiliateService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 94961:
/*!*******************************************!*\
  !*** ./src/app/services/users.service.ts ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": function() { return /* binding */ UserService; }
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 79765);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 46782);
/* harmony import */ var _utils_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/api */ 52890);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _states_app_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../states/app.state */ 99921);






class UserService {
    constructor(http, state) {
        this.http = http;
        this.state = state;
        this.subscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__.Subject();
    }
    ;
    adduser(data) {
        return this.http.post(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/users/new`, data);
    }
    login(data) {
        return this.http.post(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/users/login`, data);
    }
    update(data) {
        return this.http.put(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/users/update`, data);
    }
    updatePassword(data) {
        return this.http.put(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/users/password/update`, data);
    }
    loadUsers() {
        this.http.get(`${_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL}/users/get`)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.takeUntil)(this.subscription$))
            .subscribe((response) => {
            if (response.success) {
                this.state.users$.next(response.data);
            }
        });
    }
    ngOnDestroy() {
        this.subscription$.next();
        this.subscription$.complete();
    }
}
UserService.ɵfac = function UserService_Factory(t) { return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_states_app_state__WEBPACK_IMPORTED_MODULE_1__.AppState)); };
UserService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: UserService, factory: UserService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 52890:
/*!******************************!*\
  !*** ./src/app/utils/api.ts ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BASEURL": function() { return /* binding */ BASEURL; }
/* harmony export */ });
const BASEURL = `http://172.105.22.95/afm`;


/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map