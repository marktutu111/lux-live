(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (self["webpackChunkngx_admin"] = self["webpackChunkngx_admin"] || []).push([["common"], {
    /***/
    56425:
    /*!************************************************!*\
      !*** ./src/app/services/affiliates.service.ts ***!
      \************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AffiliateService": function AffiliateService() {
          return (
            /* binding */
            _AffiliateService
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      79765);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      46782);
      /* harmony import */


      var _utils_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../utils/api */
      52890);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      91841);
      /* harmony import */


      var _states_app_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../states/app.state */
      99921);

      var _AffiliateService = /*#__PURE__*/function () {
        function _AffiliateService(http, state) {
          _classCallCheck(this, _AffiliateService);

          this.http = http;
          this.state = state;
          this.subscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__.Subject();
        }

        _createClass(_AffiliateService, [{
          key: "add",
          value: function add(data) {
            return this.http.post("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/affiliates/new"), data);
          }
        }, {
          key: "login",
          value: function login(data) {
            return this.http.post("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/affiliates/login"), data);
          }
        }, {
          key: "updatePassword",
          value: function updatePassword(data) {
            return this.http.put("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/affiliates/password/update"), data);
          }
        }, {
          key: "update",
          value: function update(data) {
            return this.http.put("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/affiliates/update"), data);
          }
        }, {
          key: "getall",
          value: function getall() {
            var _this = this;

            this.http.get("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/affiliates/get")).pipe((0, rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.takeUntil)(this.subscription$)).subscribe(function (response) {
              if (response.success) {
                _this.state.setAffiliates(response.data);
              }
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subscription$.next();
            this.subscription$.complete();
          }
        }]);

        return _AffiliateService;
      }();

      _AffiliateService.ɵfac = function AffiliateService_Factory(t) {
        return new (t || _AffiliateService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_states_app_state__WEBPACK_IMPORTED_MODULE_1__.AppState));
      };

      _AffiliateService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({
        token: _AffiliateService,
        factory: _AffiliateService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    94961:
    /*!*******************************************!*\
      !*** ./src/app/services/users.service.ts ***!
      \*******************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "UserService": function UserService() {
          return (
            /* binding */
            _UserService
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      79765);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      46782);
      /* harmony import */


      var _utils_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../utils/api */
      52890);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      91841);
      /* harmony import */


      var _states_app_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../states/app.state */
      99921);

      var _UserService = /*#__PURE__*/function () {
        function _UserService(http, state) {
          _classCallCheck(this, _UserService);

          this.http = http;
          this.state = state;
          this.subscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__.Subject();
        }

        _createClass(_UserService, [{
          key: "adduser",
          value: function adduser(data) {
            return this.http.post("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/users/new"), data);
          }
        }, {
          key: "login",
          value: function login(data) {
            return this.http.post("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/users/login"), data);
          }
        }, {
          key: "update",
          value: function update(data) {
            return this.http.put("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/users/update"), data);
          }
        }, {
          key: "updatePassword",
          value: function updatePassword(data) {
            return this.http.put("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/users/password/update"), data);
          }
        }, {
          key: "loadUsers",
          value: function loadUsers() {
            var _this2 = this;

            this.http.get("".concat(_utils_api__WEBPACK_IMPORTED_MODULE_0__.BASEURL, "/users/get")).pipe((0, rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.takeUntil)(this.subscription$)).subscribe(function (response) {
              if (response.success) {
                _this2.state.users$.next(response.data);
              }
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subscription$.next();
            this.subscription$.complete();
          }
        }]);

        return _UserService;
      }();

      _UserService.ɵfac = function UserService_Factory(t) {
        return new (t || _UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_states_app_state__WEBPACK_IMPORTED_MODULE_1__.AppState));
      };

      _UserService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({
        token: _UserService,
        factory: _UserService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    52890:
    /*!******************************!*\
      !*** ./src/app/utils/api.ts ***!
      \******************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "BASEURL": function BASEURL() {
          return (
            /* binding */
            _BASEURL
          );
        }
        /* harmony export */

      });

      var _BASEURL = "http://172.105.22.95/afm";
      /***/
    }
  }]);
})();
//# sourceMappingURL=common-es5.js.map